package com.folcademy.clinica.Model.Entities;

import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Objects;

@Entity
@Table (name = "Turno")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@AllArgsConstructor

public class Turno {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idturno", columnDefinition = "INT(10) UNSIGNED")
    private Integer idturno;
    @Column(name = "fecha", columnDefinition = "DATE")
    private LocalDate fecha;
    @Column(name = "hora", columnDefinition = "TIME")
    private LocalTime hora;
    @Column(name = "atendido", columnDefinition = "TINYINT")
    private Boolean atendido;
    @Column(name = "idpaciente", columnDefinition = "INT")
    private Integer idpaciente;
    @Column(name = "idmedico", columnDefinition = "INT")
    private Integer idmedico;

//    @ManyToOne
//    @NotFound(action = NotFoundAction.IGNORE)
//    @JoinColumn(name = "idpaciente",referencedColumnName = "idpaciente",insertable = false,updatable = false)
//    private Paciente paciente;
//
//
//    @ManyToOne
//    @NotFound(action = NotFoundAction.IGNORE)
//    @JoinColumn(name = "idmedico",referencedColumnName = "idmedico",insertable = false,updatable = false)
//    private Medico medico;
//
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Turno turno = (Turno) o;
        return idturno != null && Objects.equals(idturno, turno.idturno);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
package com.folcademy.clinica.Model.Dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EditMedicoDTO {
    private Integer idmed;
    private String nombre;
    private String apellido;
    private String profesion;
    private Integer consulta;
}

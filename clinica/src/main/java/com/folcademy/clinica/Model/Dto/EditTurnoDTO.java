package com.folcademy.clinica.Model.Dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EditTurnoDTO {
    private LocalDate fecha;
    private LocalTime hora;
    private Boolean atendido;
    private Integer idpaciente;
    private Integer idmedico;
}

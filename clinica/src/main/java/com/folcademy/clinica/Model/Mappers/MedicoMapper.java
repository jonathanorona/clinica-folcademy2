package com.folcademy.clinica.Model.Mappers;

import com.folcademy.clinica.Model.Dto.EditMedicoDTO;
import com.folcademy.clinica.Model.Dto.NewMedicoDTO;
import com.folcademy.clinica.Model.Entities.Medico;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class MedicoMapper {
    public NewMedicoDTO entityToDto(Medico entity) {
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new NewMedicoDTO(
                                ent.getIdmed(),
                                ent.getNombre(),
                                ent.getApellido(),
                                ent.getProfesion(),
                                ent.getConsulta()
                        )
                )
                .orElse(new NewMedicoDTO());
    }

    public Medico dtoToEntity(Medico dto) {
        Medico entity = new Medico();
        entity.setIdmed(dto.getIdmed());
        entity.setProfesion(dto.getProfesion());
        entity.setConsulta(dto.getConsulta());
        return entity;
    }

    public EditMedicoDTO entityToEnteroDto(Medico entity) {
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new EditMedicoDTO(
                                ent.getIdmed(),
                                ent.getNombre(),
                                ent.getApellido(),
                                ent.getProfesion(),
                                ent.getConsulta()
                        )
                )
                .orElse(new EditMedicoDTO());
    }

    public Medico enteroDtoToEntity(EditMedicoDTO dto) {
        Medico entity = new Medico();
        entity.setIdmed(dto.getIdmed());
        entity.setNombre(dto.getNombre());
        entity.setApellido(dto.getApellido());
        entity.setProfesion(dto.getProfesion());
        entity.setConsulta(dto.getConsulta());
        return entity;
    }
}


package com.folcademy.clinica.Services;

import com.folcademy.clinica.Exceptions.BadRequestException;
import com.folcademy.clinica.Exceptions.NotFoundException;
import com.folcademy.clinica.Model.Dto.EditPacienteDTO;
import com.folcademy.clinica.Model.Dto.NewPacienteDTO;
import com.folcademy.clinica.Model.Entities.Paciente;
import com.folcademy.clinica.Model.Repositories.MedicoRepository;
import com.folcademy.clinica.Model.Repositories.PacienteRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.naming.NamingException;
import java.util.List;
import java.util.Optional;

@Service
public class PacienteService {
    private final PacienteRepository pacienteRepository;

    public PacienteService(PacienteRepository pacienteRepository) {
        this.pacienteRepository = pacienteRepository;
    }

    //listar pacientes
    public List<Paciente> listarTodos() {
        if (pacienteRepository.findAll() == null){
            throw new NotFoundException("No se encontró ningún paciente");
        } else {
            return pacienteRepository.findAll();
        }
    }


    //crear paciente
    public ResponseEntity<?> crearPaciente(NewPacienteDTO newPacienteDTO){
        if (newPacienteDTO.getNombre() == null){
            throw new BadRequestException("El nombre es obligatorio");
        }

        Paciente pacienteEntity = new Paciente();
        pacienteEntity.setNombre(newPacienteDTO.getNombre());
        pacienteEntity.setApellido(newPacienteDTO.getApellido());
        pacienteEntity.setDni(newPacienteDTO.getDni());
        pacienteEntity.setTelefono(newPacienteDTO.getTelefono());

        pacienteRepository.save(pacienteEntity);

        return new ResponseEntity<>(pacienteEntity, HttpStatus.OK);
    }

    //eliminar paciente
    public ResponseEntity<?> deletePaciente(Integer id){

        Optional<Paciente> pacienteOptional = pacienteRepository.findById(id);
        if (pacienteOptional.isEmpty()){
            throw new BadRequestException ("No existe un paciente con ese id");
        }

        Paciente paciente = pacienteOptional.get();
        pacienteRepository.delete(paciente);
        return new ResponseEntity<>("Paciente eliminado",HttpStatus.OK);
    }

    //editar paciente
    public ResponseEntity<?> editPaciente(Integer id, EditPacienteDTO editPacienteDTO){

        Optional<Paciente> pacienteOptional = pacienteRepository.findById(id);
        if (pacienteOptional.isEmpty()){
            throw new BadRequestException("El paciente que quiere editar no existe");
        }
        Paciente paciente = pacienteOptional.get();

        if (editPacienteDTO.getDni() != null) {
            paciente.setDni(editPacienteDTO.getDni());
        } if (editPacienteDTO.getNombre() != null){
            paciente.setNombre(editPacienteDTO.getNombre());
        } if (editPacienteDTO.getApellido() != null){
            paciente.setApellido(editPacienteDTO.getApellido());
        } if (editPacienteDTO.getTelefono() != null){
            paciente.setTelefono(editPacienteDTO.getTelefono());
        } else {
            throw new BadRequestException("Por favor escribe lo que quieres editar");
        }

        pacienteRepository.save(paciente);
        return new ResponseEntity<>("Paciente editado",HttpStatus.OK);
    }

}
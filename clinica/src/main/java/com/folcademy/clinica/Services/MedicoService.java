package com.folcademy.clinica.Services;

import com.folcademy.clinica.Exceptions.BadRequestException;
import com.folcademy.clinica.Exceptions.NotFoundException;
import com.folcademy.clinica.Model.Dto.MedicoEnteroDTO;
import com.folcademy.clinica.Model.Dto.NewMedicoDTO;
import com.folcademy.clinica.Model.Entities.Medico;
import com.folcademy.clinica.Model.Entities.Paciente;
import com.folcademy.clinica.Model.Mappers.MedicoMapper;
import com.folcademy.clinica.Model.Repositories.MedicoRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service("medicoService")
public class MedicoService {
    private final MedicoRepository medicoRepository;
    private final MedicoMapper medicoMapper;

    public MedicoService(MedicoRepository medicoRepository, MedicoMapper medicoMapper) {
        this.medicoRepository = medicoRepository;
        this.medicoMapper = medicoMapper;
    }

    //listar medicos
    public List<Medico> listarTodos() {
        if (medicoRepository.findAll() == null){
            throw new NotFoundException("No se encontró ningún medico");
        } else {
            return medicoRepository.findAll();
        }
    }


    //crear medico
    public ResponseEntity<?> crearMedico(NewMedicoDTO newMedicoDTO){

        Medico medicoEntity = new Medico();
        medicoEntity.setNombre(newMedicoDTO.getNombre());
        medicoEntity.setApellido(newMedicoDTO.getApellido());
        medicoEntity.setProfesion(newMedicoDTO.getProfesion());
        medicoEntity.setConsulta(newMedicoDTO.getConsulta());
        if(medicoEntity.getConsulta()<0)
            throw new BadRequestException("La consulta no puede ser menor a 0");

        medicoRepository.save(medicoEntity);

        return new ResponseEntity<>(medicoEntity, HttpStatus.OK);
    }

    //eliminar medico
    public ResponseEntity<?> deleteMedico(Integer idmed){

        Optional<Medico> medicoOptional = medicoRepository.findById(idmed);
        if (medicoOptional.isEmpty()){
            throw new BadRequestException("No existe un medico con ese id");
        }

        Medico medico = medicoOptional.get();
        medicoRepository.delete(medico);
        return new ResponseEntity<>("Medico eliminado",HttpStatus.OK);
    }

    //editar medico
    public ResponseEntity<?> editMedico(Integer idmed, MedicoEnteroDTO editMedicoDTO){
//        try {
            Optional<Medico> medicoOptional = medicoRepository.findById(idmed);
            if (medicoOptional.isEmpty()){
                throw new BadRequestException("El medico que quiere editar no existe");
            }
            Medico medico = medicoOptional.get();

            if (editMedicoDTO.getNombre() != null){
                medico.setNombre(editMedicoDTO.getNombre());
            }

            if (editMedicoDTO.getApellido() != null){
                medico.setApellido(editMedicoDTO.getApellido());
            }

            if (editMedicoDTO.getConsulta() != null){
                if (editMedicoDTO.getConsulta() >= 0){
                    medico.setConsulta(editMedicoDTO.getConsulta());
                } else {
                    throw new BadRequestException("La consulta tiene que ser mayor a 0");
                }
            }

            if (editMedicoDTO.getProfesion() != null){
                medico.setProfesion(editMedicoDTO.getProfesion());
            }

            if (editMedicoDTO == null)
                throw new BadRequestException("Por favor escribe lo que quieres editar");

            medicoRepository.save(medico);
            return new ResponseEntity<>("Medico editado",HttpStatus.OK);
//        } catch (Exception e) {
//            return new ResponseEntity<>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
//        }
    }
}




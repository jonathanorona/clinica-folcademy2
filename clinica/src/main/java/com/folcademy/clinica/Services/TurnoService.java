package com.folcademy.clinica.Services;

import com.folcademy.clinica.Exceptions.BadRequestException;
import com.folcademy.clinica.Exceptions.NotFoundException;
import com.folcademy.clinica.Model.Dto.EditTurnoDTO;
import com.folcademy.clinica.Model.Dto.NewTurnoDTO;
import com.folcademy.clinica.Model.Entities.Turno;
import com.folcademy.clinica.Model.Repositories.TurnoRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TurnoService {
    private final TurnoRepository turnoRepository;

    public TurnoService(TurnoRepository turnoRepository) {
        this.turnoRepository = turnoRepository;
    }

    //listar Turnos
    public List<Turno> listarTodos() {
        List<Turno> turnoOptional = turnoRepository.findAll();
        if (turnoOptional.isEmpty()){
            throw new NotFoundException("No se encontró ningún turno");
        } else {
            return turnoRepository.findAll();
        }
    }

    //crear Turno
    public ResponseEntity<?> crearTurno(NewTurnoDTO newTurnoDTO){
        if (newTurnoDTO.getFecha() == null){
            throw new BadRequestException("La fecha es obligatoria");
        }

        Turno turnoEntity = new Turno();
        turnoEntity.setFecha(newTurnoDTO.getFecha());
        turnoEntity.setHora(newTurnoDTO.getHora());
        turnoEntity.setAtendido(newTurnoDTO.getAtendido());
        turnoEntity.setIdpaciente(newTurnoDTO.getIdpaciente());
        turnoEntity.setIdmedico(newTurnoDTO.getIdmedico());

        turnoRepository.save(turnoEntity);

        return new ResponseEntity<>(turnoEntity, HttpStatus.OK);
    }


        //eliminar turno
    public ResponseEntity<?> deleteTurno(Integer idturno){

        Optional<Turno> turnoOptional = turnoRepository.findById(idturno);
        if (turnoOptional.isEmpty()){
            throw new BadRequestException("No existe un turno con ese id");
        }

        Turno turno = turnoOptional.get();
        turnoRepository.delete(turno);
        return new ResponseEntity<>("Turno eliminado",HttpStatus.OK);
    }

    //editar Turno
    public ResponseEntity<?> editTurno(Integer idturno, EditTurnoDTO editTurnoDTO){

        Optional<Turno> turnoOptional = turnoRepository.findById(idturno);
        if (turnoOptional.isEmpty()){
            throw new BadRequestException("El turno que quiere editar no existe");
        }
        Turno turno = turnoOptional.get();

        if (editTurnoDTO.getFecha() != null) {
            turno.setFecha(editTurnoDTO.getFecha());
        } if (editTurnoDTO.getHora() != null){
            turno.setHora(editTurnoDTO.getHora());
        } if (editTurnoDTO.getAtendido() != null){
            turno.setAtendido(editTurnoDTO.getAtendido());
        } if (editTurnoDTO.getIdpaciente() != null){
            turno.setIdpaciente(editTurnoDTO.getIdpaciente());
        } if (editTurnoDTO.getIdmedico() != null){
            turno.setIdmedico(editTurnoDTO.getIdmedico());
        } else {
            return new ResponseEntity<>("Por favor escribe lo que quieres editar",HttpStatus.BAD_REQUEST);
        }

        turnoRepository.save(turno);
        return new ResponseEntity<>("Turno editado",HttpStatus.OK);
    }



}
package com.folcademy.clinica.Exceptions;

public class ErrorMessages {
    public static final String VALIDATION_NULL_DATE = "No se ha enviado la fecha.";
    public static final String VALIDATION_NULL_NAME = "No se ha enviado el nombre.";
}

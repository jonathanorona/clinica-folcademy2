package com.folcademy.clinica.Controllers;


import com.folcademy.clinica.Model.Dto.EditTurnoDTO;
import com.folcademy.clinica.Model.Dto.NewTurnoDTO;
import com.folcademy.clinica.Model.Entities.Turno;
import com.folcademy.clinica.Services.TurnoService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/turnos")

public class TurnoController {
    private final TurnoService turnoService;

    public TurnoController(TurnoService turnoService) {
        this.turnoService = turnoService;
    }

    //list
    @GetMapping("")
    public ResponseEntity<List<Turno>> listarTodo() {
        return ResponseEntity.ok(turnoService.listarTodos());
    }

    //create
    @PostMapping("")
    public ResponseEntity<?> crearTurno(@RequestBody NewTurnoDTO newTurnoDTO) {
        return turnoService.crearTurno(newTurnoDTO);
    }

    //delete
    @DeleteMapping("")
    public ResponseEntity<?> deleteTurno(@RequestParam Integer id) {
        return turnoService.deleteTurno(id);
    }

    //edit
    @PutMapping("")
    public ResponseEntity<?> editTurno(@RequestParam Integer id, @RequestBody EditTurnoDTO editTurnoDTO) {
        return turnoService.editTurno(id, editTurnoDTO);
    }

}
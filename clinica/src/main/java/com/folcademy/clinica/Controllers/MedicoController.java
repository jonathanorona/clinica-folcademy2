package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.Dto.MedicoEnteroDTO;
import com.folcademy.clinica.Model.Dto.NewMedicoDTO;
import com.folcademy.clinica.Model.Entities.Medico;
import com.folcademy.clinica.Services.MedicoService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/medicos")

public class MedicoController {
    private final MedicoService medicoService;

    public MedicoController(MedicoService medicoService) {
        this.medicoService = medicoService;
    }

    //list
    @GetMapping("")
    public ResponseEntity<List<Medico>> listarTodo() {
        return ResponseEntity.ok(medicoService.listarTodos());
    }


    //create
    @PostMapping("")
    public ResponseEntity<?> crearMedico(@RequestBody NewMedicoDTO newMedicoDTO){
        return medicoService.crearMedico(newMedicoDTO);
    }

    //delete
    @DeleteMapping("")
    public ResponseEntity<?> deleteMedico(@RequestParam(name = "id") Integer idmed) {
        return medicoService.deleteMedico(idmed);
    }

    //edit
    @PutMapping("")
    public ResponseEntity<?> editMedico(@RequestParam Integer id, @RequestBody MedicoEnteroDTO editMedicoDTO){
        return medicoService.editMedico(id, editMedicoDTO);
    }


}

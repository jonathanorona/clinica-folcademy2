package com.folcademy.clinica.Controllers;


import com.folcademy.clinica.Model.Dto.EditPacienteDTO;
import com.folcademy.clinica.Model.Dto.NewPacienteDTO;
import com.folcademy.clinica.Model.Entities.Paciente;
import com.folcademy.clinica.Services.PacienteService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/pacientes")

public class PacienteController {
    private final PacienteService pacienteService;

    public PacienteController(PacienteService pacienteService) {
        this.pacienteService = pacienteService;
    }

    //list
    @GetMapping("")
    public ResponseEntity<List<Paciente>> listarTodo() {
        return ResponseEntity.ok(pacienteService.listarTodos());
    }

    //create
    @PostMapping("")
    public ResponseEntity<?> crearPaciente(@RequestBody NewPacienteDTO newPacienteDTO){
        return pacienteService.crearPaciente(newPacienteDTO);
    }

    //delete
    @DeleteMapping("")
    public ResponseEntity<?> deletePaciente(@RequestParam Integer id){
        return pacienteService.deletePaciente(id);
    }

    //edit
    @PutMapping("")
    public ResponseEntity<?> editPaciente(@RequestParam Integer id, @RequestBody EditPacienteDTO editPacienteDTO){
        return pacienteService.editPaciente(id, editPacienteDTO);
    }
}
